# Backend Test

Create a NodeJS application with MYSQL or SQLITE3 database. NodeJS application should provide below operations.
-	Populate database with the, user table (email and password ), countryDetail (CountryName, GMTOffset)
-	Login API, email and password validation, Access token should be sent in response. NOTE: Access token should be valid upto 5min.
-	getAllCountryDetail, Token as Auth validation, API should return all the country & its GMT Offset detail in response
-	getCountryDetail, Token as Auth & Country Name, API should return requested country GMT Offset if found, otherwise the error code
-	refreshToken, API should renew the existing token if it expires instead of making new token request
-	Application should be running in containerization environment
